// https://leetcode.com/problems/single-number/

class Solution {
    public int singleNumber(int[] nums) {
    // 117 ms, faster than 5.01%
        ArrayList<Integer> findNum = new ArrayList<Integer>();

        for (int i = 0; i < nums.length; i++)
        {
            if (findNum.contains(nums[i]))
                findNum.remove((Integer) nums[i]);
            else
                findNum.add((Integer) nums[i]);
        }

        return findNum.get(0);
    }
}
