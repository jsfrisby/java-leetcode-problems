// https://leetcode.com/problems/sort-an-array/

class Solution {
    public int[] sortArray(int[] nums) {
        int len = nums.length;
        // let's use the fastest sorting algorithm
        mergeSort(nums, 0, len - 1);
        return nums;
    }
    
    
    void mergeSort(int[] nums, int start, int end)
    {
        if (end - start + 1 <= 1)
            return; // Array is already sorted.
        
        int mid = start + (end - start) / 2;
        
        // left side
        mergeSort(nums, start, mid);
        // right side
        mergeSort(nums, mid + 1, end);
        
        // combine
        merge(nums, start, mid, end);
    }
    
    void merge(int[] nums, int start, int mid, int end){
        int left = start;
        int right = mid + 1;
        int[] buffer = new int[end - start + 1];
        int temp = 0; // buffer pointer
        
        while (left <= mid && right <= end)
        {
            if (nums[left] < nums[right])
            {
                buffer[temp++] = nums[left++];
            }
            else 
            {
                buffer[temp++] = nums[right++];
            }
        }
        
        while (left <= mid)
        {
            buffer[temp++] = nums[left++];
        }
        
        while (right <= end)
        {
            buffer[temp++] = nums[right++];
        }
        
        // copy sorted buffer into original array
        for (int i = start; i <= end; i++)
        {
            nums[i] = buffer[i - start];
        }
    }   
}