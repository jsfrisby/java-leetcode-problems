// https://leetcode.com/problems/reverse-integer/

class Solution {
    public int reverse(int x) {
        String num = Integer.toString(x);
        String rev = "";
        boolean neg = false;

        if (x < 0) {
            neg = true;
            num = num.substring(1);
        }

        for(int i = num.length() - 1; i >= 0; i--)
        {
            rev = rev + num.charAt(i);
        }

        if (neg)
        {
            rev = "-" + rev;
        }

        try
        {
            return Integer.parseInt(rev);
        } catch (Exception e)
        {
            System.out.println(e);
            return 0;
        }
    }
}
