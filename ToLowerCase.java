// https://leetcode.com/problems/to-lower-case/

class Solution {
    public String toLowerCase(String str) {
        // 0 ms, faster than 100%
        //return str.toLowerCase();

        // 0 ms, faster than 100%
        String buildStr = "";

        for (int i = 0; i < str.length(); i++)
        {
            buildStr += Character.toLowerCase(str.charAt(i));
        }

        return buildStr;
    }
}
