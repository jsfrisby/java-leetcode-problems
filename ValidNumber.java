// https://leetcode.com/problems/valid-number/

class Solution {
    public boolean isNumber(String s) {
        if (s.contains("f") || s.contains("F"))
        {
            return false;
            // try
            // {
            //     float f = Float.parseFloat(s);
            //     System.out.println("f");
            //     return false;
            // } catch (Exception e) {
            //     return false;
            // }
        }
        else
        {
            try
            {
                if ((s.contains("d") || s.contains("D")) && s.contains("e"))
                {
                    return false;
                }
                else if (s.contains("D"))
                {
                    return false;
                }
                else
                {
                    double dec = Double.parseDouble(s);
                    System.out.println("d");
                    return true;
                }
            } catch (Exception e) {
                return false;
            }
        }
    }
}
