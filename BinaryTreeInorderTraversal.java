// https://leetcode.com/problems/binary-tree-inorder-traversal/

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List <Integer> list = new ArrayList<Integer>();
        traverse(root, list);
        return list;
    }

    public void traverse(TreeNode root, List<Integer> list)
    {
        if (root == null)
            return;

        traverse(root.left, list);
        list.add(root.val);
        traverse(root.right, list);
    }
}
