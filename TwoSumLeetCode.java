// https://leetcode.com/problems/two-sum/

class Solution {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
        int[] indices = new int[2];
        
        for (int i = 0; i < nums.length; i++)
        {
            hm.put(nums[i], i);
        }
        
        System.out.println(hm);
        
        for (int i = 0; i < nums.length; i++)
        {
            int num2 = target - nums[i];
            System.out.println(i);
            System.out.println(num2);
            
            if (hm.containsKey(num2) && i != hm.get(num2))
            {
                indices[0] = i;
                indices[1] = hm.get(num2);
                return indices;
            }
        }
        
        return indices;
    }
}