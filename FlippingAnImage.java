// https://leetcode.com/problems/flipping-an-image/

class Solution {
    public int[][] flipAndInvertImage(int[][] A) {
        // 3 ms, faster than 36.42%
        // 37.1 MB, less than 100.00% of Java online submissions for Flipping an Image.
        String row;

        // // print original matrix
        // for (int i = 0; i < A.length; i++)
        // {
        //     System.out.println("\nRow: " + i);
        //     for (int j = 0; j < A[i].length; j++)
        //     {
        //         System.out.print(A[i][j]);
        //     }
        // }

        // reverse rows in the array
        for (int i = 0; i < A.length; i++)
        {
            row = "";
            for (int j = A[i].length - 1; j >= 0; j--)
            {
                // System.out.println(j);
                row = row + A[i][j] + "";
                // System.out.println("row=" + row);
            }

            for (int j = 0; j <= A[i].length - 1; j++)
            {
                A[i][j] = Integer.parseInt(row.charAt(j) + "");
            }
        }

        // // check reverse
        // for (int i = 0; i < A.length; i++)
        // {
        //     System.out.println("\nRow: " + i);
        //     for (int j = 0; j < A[i].length; j++)
        //     {
        //         System.out.print(A[i][j]);
        //     }
        // }

        // invert the array
        for (int i = 0; i < A.length; i++)
        {
            for (int j = 0; j < A[i].length; j++)
            {
                if (A[i][j] == 0)
                    A[i][j] = 1;
                else
                    A[i][j] = 0;
            }
        }

        return A;
    }
}
