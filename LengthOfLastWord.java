// https://leetcode.com/problems/length-of-last-word/

class Solution {
    public int lengthOfLastWord(String s) {
        s = s.trim();
        int lastIndex = s.lastIndexOf(" ");

        // System.out.println(lastIndex);
        String sub = s.substring(lastIndex+1);
        // System.out.println(sub);
        // int len = sub.length();
        // return len;
        return sub.length();
    }
}
