// https://leetcode.com/problems/range-sum-of-bst/

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    // 1 ms, faster than 51.91%
    public int total = 0;

    public int rangeSumBST(TreeNode root, int L, int R) {
        inOrder(root, L, R);
        return total;
    }

    public void inOrder(TreeNode root, int L, int R) {
        if (root != null)
        {
            inOrder(root.left, L, R);

            if (root.val >= L && root.val <= R)
                total += root.val;

            inOrder(root.right, L, R);
        }
    }
}
