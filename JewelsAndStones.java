// https://leetcode.com/problems/jewels-and-stones/

class Solution {
    public int numJewelsInStones(String J, String S) {
        int count = 0;

        // make sure Strings J and S are not empty
        if (J.equals("") || S.equals(""))
            return 0;

        // loop through all the characters in S and
        // see if J has a character that matches
        for (int i = 0; i < S.length(); i++)
        {
            if (J.indexOf(S.charAt(i)) >= 0)
                count++;
        }

        return count;
    }
}
