// https://leetcode.com/problems/defanging-an-ip-address/

class Solution {
    public String defangIPaddr(String address) {
		String build = "";

		for (int i = 0; i < address.length(); i++)
		{
			if (address.charAt(i) == '.')
				build += "[.]";
			else
				build += address.charAt(i);
		}

        return build;

        // return address.replace(".", "[.]");
    }
}
