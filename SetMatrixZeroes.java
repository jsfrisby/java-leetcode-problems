// https://leetcode.com/problems/set-matrix-zeroes/

class Solution {
    public void setZeroes(int[][] matrix) {
        ArrayList rows = new ArrayList();
        ArrayList cols = new ArrayList();
        int rowLen = matrix.length;
        int colLen = matrix[0].length;

        for (int i = 0; i < rowLen; i++)
        {
            for(int j = 0; j < colLen; j++)
            {
                if (matrix[i][j] == 0)
                {
                    rows.add(i);
                    cols.add(j);
                }
            }
        }

        for (Object eachRow : rows)
        {
            // rows.forEach((eachRow) ->
            for (int i = 0; i < colLen; i++)
            {
                matrix[Integer.parseInt(eachRow + "")][i] = 0;
            }
        }

        for (Object eachCol : cols)
        {
            for (int i = 0; i < rowLen; i++)
            {
                matrix[i][Integer.parseInt(eachCol + "")] = 0;
            }
        }
    }
}
